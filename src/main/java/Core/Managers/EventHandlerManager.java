package Core.Managers;

import Core.Interfaces.IEventHandler;

import java.util.ArrayList;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

public class EventHandlerManager {
    private static EventHandlerManager instance = new EventHandlerManager();

    private ConcurrentHashMap<UUID, ArrayList<IEventHandler>> eventHandlerMap;

    public static EventHandlerManager Get() {
        if(instance != null) return instance;
        instance = new EventHandlerManager();
        return instance;
    }

    private EventHandlerManager() {
        eventHandlerMap = new ConcurrentHashMap<>();
    }

    public void RegisterEventHandler(IEventHandler handler) {
        UUID eventType = handler.GetHandlerEventType();
        if(!eventHandlerMap.containsKey(eventType)) {
            eventHandlerMap.put(eventType, new ArrayList<>());
        }
        eventHandlerMap.get(eventType).add(handler);

    }
    public ArrayList<IEventHandler> GetHandlersForEvent(UUID eventType) {
        return eventHandlerMap.getOrDefault(eventType, null);
    }

}
