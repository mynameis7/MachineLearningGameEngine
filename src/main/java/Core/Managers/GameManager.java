package Core.Managers;

import Core.Models.Window;
import org.lwjgl.glfw.GLFWErrorCallback;

import java.time.Duration;
import java.time.Instant;
import java.util.Objects;

import static org.lwjgl.glfw.GLFW.*;

public class GameManager {
    private static GameManager instance = new GameManager();

    public static GameManager Get() {
        if(instance != null) return instance;
        instance = new GameManager();
        return instance;
    }

    private Instant lastTime;

    private GameManager() {

    }

    public void Run() {
        GLFWErrorCallback errorCB = GLFWErrorCallback.createPrint(System.err);
        glfwSetErrorCallback(errorCB);

        System.err.println("---- [ Error callback test ] ----");
        glfwDefaultWindowHints();
        System.err.println("---- [ Error callback done ] ----");
        System.err.flush();

        if (!glfwInit()) {
            throw new IllegalStateException("Failed to initialize GLFW.");
        }

        System.out.println("GLFW initialized");

        try {
            lastTime = Instant.now();
            new Window(800, 640, "Test");
            //new Window(800, 640, "Test2");

            while(HasWindows()) {
                Loop();
            }

        } finally {
            glfwTerminate();
            Objects.requireNonNull(glfwSetErrorCallback(null)).free();
        }
        CleanUp();
    }

    public void Loop() {

        EventManager.Get().ProcessEvents();
        Duration dt = Duration.between(Instant.now(), lastTime);
        GameObjectManager.Get().Update(dt);
        WindowManager.Get().ProcessWindows();
        lastTime = Instant.now();
    }

    public void CleanUp() {
        WindowManager.Get().CleanUp();

    }

    public boolean HasWindows() {
        return WindowManager.Get().HasWindows();
    }
}
