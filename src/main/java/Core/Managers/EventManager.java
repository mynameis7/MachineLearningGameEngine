package Core.Managers;

import Core.Constants.EventType;
import Core.Models.Event;

import java.time.Instant;
import java.util.UUID;
import java.util.concurrent.*;

public class EventManager {
    private static EventManager instance = new EventManager();

    private ConcurrentHashMap<EventType, UUID> eventTypeMappings;

    public static EventManager Get() {
        if(instance != null) return instance;
        instance = new EventManager();
        return instance;
    }

    private ConcurrentLinkedQueue<Event> eventQueue;
    private EventManager() {
        eventQueue = new ConcurrentLinkedQueue<Event>();
        eventTypeMappings = new ConcurrentHashMap<>();
        var eventTypes = EventType.values();
        for(int i = 0; i < eventTypes.length; i++) {
            EventType type = eventTypes[i];
            eventTypeMappings.put(type, UUID.randomUUID());
        }
    }

    public void ProcessEvents() {
        Instant currentTime = Instant.now();
        Event nextEvent = eventQueue.peek();
        while(nextEvent !=  null && nextEvent.timestamp.isBefore(currentTime)) {
            var currentEvent = eventQueue.poll();
            var handlers = EventHandlerManager.Get().GetHandlersForEvent(currentEvent.eventType);
            handlers.forEach((handler) -> ThreadPoolManager.Get().submit( () -> handler.Handle(currentEvent, handler.GetRunnable())));
            nextEvent = eventQueue.peek();
        }
    }


    public void Add(Event e) {
        eventQueue.add(e);
    }

    public UUID GetEventType(EventType type) {
        return eventTypeMappings.get(type);
    }
}
