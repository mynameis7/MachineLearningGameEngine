package Core.Managers;

import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class ThreadPoolManager extends ThreadPoolExecutor{
    private static ThreadPoolManager instance = new ThreadPoolManager();

    public static ThreadPoolManager Get() {
        if(instance != null) return instance;
        instance = new ThreadPoolManager();
        return instance;
    }

    private ThreadPoolManager() {
        super(1, Integer.MAX_VALUE, 1, TimeUnit.SECONDS, new LinkedBlockingQueue<Runnable>());
        this.setRejectedExecutionHandler(new ThreadPoolExecutor.CallerRunsPolicy());
    }
}
