package Core.Managers;

import Core.Models.Window;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicBoolean;

public class WindowManager {
    private static WindowManager instance = new WindowManager();

    public static WindowManager Get() {
        if(instance != null) return instance;
        instance = new WindowManager();
        return instance;
    }

    ConcurrentHashMap<Long, Window> windows;
    private WindowManager() {
        windows = new ConcurrentHashMap<>();
    }

    public void Add(Window w) {
        windows.put(w.GetHandle(), w);
    }

    public void ProcessWindows() {

        windows.forEach((handle, window) -> {
            window.update();
        });
    }

    public void CleanUp() {
        windows.forEach((aLong, window) -> {if(window.IsOpen())window.Destroy();});
    }

    public boolean HasWindows() {
        AtomicBoolean hasWindows = new AtomicBoolean(false);
        windows.forEach((aLong, window) -> {if(window.IsOpen()) { hasWindows.set(true);} });
        return hasWindows.getOpaque();
    }
}
