package Core.Managers;

import Core.GameObject;
import Core.Interfaces.IComponent;

import java.time.Duration;
import java.util.ArrayList;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;



public class GameObjectManager {
    private static GameObjectManager instance = new GameObjectManager();

    public static GameObjectManager Get() {
        if(instance != null) return instance;
        instance = new GameObjectManager();
        return instance;
    }

    private ConcurrentHashMap<UUID, GameObject> objects;
    private ConcurrentHashMap<UUID, ArrayList<IComponent>> components;

    private GameObjectManager() {
        objects = new ConcurrentHashMap<>();
        components = new ConcurrentHashMap<>();
    }

    public void AddGameObject(GameObject o) {
        objects.put(o.GetId(), o);
    }

    public GameObject GetObject(UUID id) {
        return objects.get(id);
    }

    public ArrayList<IComponent> GetComponents(UUID id) {
        return components.get(id);
    }


    public void Update(Duration dt) {
        double ddt = dt.toMillis()/1000.0;
        components.forEach((uuid, iComponents) -> {
            ThreadPoolManager.Get().execute(() -> {
                iComponents.forEach(iComponent -> {
                    ThreadPoolManager.Get().submit(() -> iComponent.update(dt, ddt));
                });
            });
        });
    }
}
