package Core.Interfaces;

import java.time.Duration;

public interface IComponent {
    void update(Duration dt, double secondsDelta);
}
