package Core.Interfaces;

import Core.Models.Event;

import java.util.UUID;

public interface IEventHandler {
    UUID GetHandlerEventType();
    void Handle(Event e, Runnable r);
    Runnable GetRunnable();
}
