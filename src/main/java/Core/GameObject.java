package Core;

import Core.Interfaces.IComponent;
import Core.Managers.GameObjectManager;

import java.util.ArrayList;
import java.util.UUID;

public class GameObject {
    private final UUID id;

    public GameObject() {
        id = UUID.randomUUID();
        GameObjectManager.Get().AddGameObject(this);
    }

    public UUID GetId() {
        return id;
    }

    public ArrayList<IComponent> GetComponents() {
        return GameObjectManager.Get().GetComponents(this.id);
    }

    public void AddComponent(IComponent component) {
        GameObjectManager.Get().GetComponents(this.id).add(component);
    }
}
