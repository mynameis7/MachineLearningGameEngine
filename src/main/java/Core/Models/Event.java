package Core.Models;

import java.time.Instant;
import java.util.UUID;

public class Event {
    public final UUID id;
    public final UUID eventType;
    public final Instant timestamp;

    public Event(UUID eventType) {
        id = UUID.randomUUID();
        this.eventType = eventType;
        timestamp = Instant.now();
    }

}
